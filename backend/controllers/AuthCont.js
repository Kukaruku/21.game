const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development)
const { validationResult } = require("express-validator");
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

const banUpdate = (user) => {
    knex("user")
        .where({
        name: user
        })
        .update({
        isBanned: true
        })
}

const register = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json(errors.array())
        }
    
        const password = req.body.password
        const salt = await bcrypt.genSalt(10)
        const passwordHash = await bcrypt.hash(password, salt)
    
        knex("users")
            .insert([{
                name: req.body.name, 
                email: req.body.email, 
                password: passwordHash,
                avatarUrl: req.body.avatarUrl
            }], ["id", "email"])
            .then((users) => {
                const token = jwt.sign({
                    userId: users[0].id
                }, "secret123",
                {
                    expiresIn: "30d"
                })
                res.json({...users, token})
            })
            .catch((err) => {
                console.error(err);
                console.log(err)
                if (err.constraint === "Такая почта уже существует") {
                    return res.json({
                        sucsess: false, 
                        message: "Неверная почта"
                    })
                }
                else if (err.constraint === "users_name_unique") {
                    return res.json({
                        sucsess: false, 
                        message: "Такой никнейм уже существует"
                    })
                }
                return res.json({
                    sucsess: false, 
                    message: "Неверные данные"
                })
            })
        

        
    
    } 
    catch (err) {
        return res.status(500).json({
            message: "Не удалось зарегистрироваться"
        })
    }
}

const login = async (req, res) => {
    try {
        knex("users")
            .where({
                email: req.body.email
            })
            .select()
            .then(async (users) => {
                const user = users[0]
                console.log(user)
                if (!user) {
                    return res.status(404).json({
                        message: "Пользователь не найдет"
                    })
                }

                const isValidPass = await bcrypt.compare(req.body.password, user.password)

                if (!isValidPass) {
                    return res.status(404).json({
                        message: "Неверный логин или пароль"
                    })
                }
                const token = jwt.sign(
                {
                    userId: users[0].id
                }, 
                "secret123",
                {
                    expiresIn: "30d"
                })

                res.json({...user, token})
            })
    } catch (err) {
        res.status(500).json({
            message: "Не удалось авторизоваться"
        })
    }
}

const getMe = (req, res) => {
    try {
        knex("users")
            .where({
                id: req.userId
            })
            .select()
            .then(async (users) => {
                const user = await users[0]
                if (!user) {
                    return res.status(404).json({
                        message: "Пользователь не найден"
                    })
                }
                res.json(user)
            })
    } catch (err) {
        console.log(err)
        res.json({message: "netdostupa"})
    }
}

module.exports = {getMe, login, register, banUpdate}