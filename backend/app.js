const express = require("express");
const {registerValidation} = require("./validations/auth");
const checkAuth = require("./utils/checkAuth")
const { register, getMe, login, banUpdate } = require("./controllers/AuthCont")
const multer = require("multer")
const cors = require("cors")




const app = express()
const port = 5000

Classes = require("./game/game")
// GameItem = require("./game/game")
const game = new Classes.Game21()
const gameItem = new Classes.GameItem()


const { Server } = require("socket.io");
const io = new Server({
    cors: {
      origin: "http://localhost:3000"
    }
  });
io.listen(4000);

const io2 = new Server({
    cors: {
      origin: "http://localhost:3000"
    }
});
io2.listen(4001);


io.on("connection", (socket) => {
    socket.on("messageTo", (msg) => {
        io.emit("messageFrom", msg)
    })

    socket.on("banUser", (user) => {
      banUpdate(user)
    })


    // let counter = 0
    // setInterval(() => {
    //     // отправляем данные клиенту
    //     io.emit('hello', ++counter);
    //   }, 1000)
})

io2.on("connection", (socket) => {
  socket.on("userIdOn", (id) => {
    game.free.push(id)   
  })
  socket.on("userIdOff", (id) => {
    let freeArr = game.free.filter(nmb => nmb !== id)
    game.free = freeArr
  })
  socket.on("start", async (id) => {
    console.log("started")
    if (game.free.length > 0) {
      let opp = String(game.free.shift())
      console.log("user: " + id)
      let roomId = id + opp
      game.games.push({roomId: roomId, users: {opp: opp, user: id}})
      let socketes = await io2.fetchSockets()
      socket.emit("firstTurn", true)
      socketes.forEach(socketers => {
        socketers.join(roomId)
        console.log(socketers.id + "connected!!")
        game.users.push(socketers.id)
        socketers.emit("ready", roomId);
      });
      
      game.free = []
      console.log(game.users)

    } else { game.free.push(id)}
  })
  socket.on("userData", (data) => {
    socket.broadcast.emit("userDataOff", data)
  })
  socket.on("playerTurn", (top_card, player_cards) => {
    socket.broadcast.emit("oppTurn", top_card, player_cards)
  })

  socket.on("step", () => {
    socket.broadcast.emit("step")
  })

  socket.on("aceChange", (index, gameId) => {
    socket.broadcast.to(gameId).emit("aceChange", index)
  })

  socket.on("disconnect", () => {
    console.log("running")
    game.getGameId(socket.id, async (roomId, opp) => {
      let socketers = await io2.fetchSockets()
      socketers.forEach(socketers => {
        if (socketers.id === opp) {
          // delete game.users.find((el) => el === socketers.id)
          console.log("deleting " + opp )
          console.log("deleting " + socketers.id )
          game.users.splice(game.users.indexOf(socketers.id), 1)
          // this.users.splice(this.users.indexOf(user), 1)
          console.log("deleted" )
          socketers.emit("exit")
        }  
        socketers.leave(roomId)
      });
    })

  })



  // socket.on("playerTurn", )
})








const storage = multer.diskStorage({
    destination: (_, __, cb) => {
        cb(null, "uploads");
    },
    filename: (_, file, cb) => {
        cb(null, file.originalname)
    },
});

const upload = multer({storage})

app.use(express.json())
app.use(cors())
app.use("/uploads", express.static("uploads"))



app.post("/auth", login)
app.post("/register", registerValidation, register)

app.post("/upload", checkAuth, upload.single("image"), (req, res) => {
    res.json({
        url: `/uploads/${req.file.originalname}`
    })
})

app.get("/profile", checkAuth, getMe)


// app.get("/", (req,res) => {s
//     res.send("hello")
// })

app.listen(port, () => {
    console.log("SERVER OK")
})