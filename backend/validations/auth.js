const {body} = require("express-validator");

const registerValidation = [
    body("email", "Неверный формат почты").isEmail(),
    body("password", "Пароль должен иметь как минимум 4 символа").isLength({ min: 4 }),
    body("name", "Укажите имя").isLength({ min: 3 }),
    body("avatarUrl", "Неверная ссылка на аватарку").optional().isURL(),
]

module.exports = { registerValidation }