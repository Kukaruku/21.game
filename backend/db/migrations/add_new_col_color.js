exports.up = function(knex) {
    return knex.schema.alterTable("users", function (table) {
      table.string("color").notNullable().defaultTo("#7ac7e1")
    })
  };
  
  
exports.down = function(knex) {
    return knex.schema.alterTable("users", function (table) {
        table.dropColumn("color")
    })
};