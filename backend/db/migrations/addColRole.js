exports.up = function(knex) {
    return knex.schema.alterTable("users", function (table) {
      table.string("role")
    })
  };
  
  
exports.down = function(knex) {
    return knex.schema.alterTable("users", function (table) {
        table.dropColumn("role")
    })
};