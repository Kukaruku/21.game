
exports.up = function(knex) {
    return knex.schema
      .createTable('users', function (table) {
        table.increments('id');
        table.string('email', 255).unique().notNullable();
        table.string('name', 255).unique().notNullable();
        table.string('password', 255).notNullable();
        table.string('avatarUrl', 255);
        table.integer('experience', 255).notNullable().defaultTo(0);
        table.integer("level").notNullable().defaultTo(0)
        table.string("choosen_perk")
        table.timestamps(true, true);
      });
  };
  
exports.down = function(knex) {
  return knex.schema
    .dropTable('users');
};