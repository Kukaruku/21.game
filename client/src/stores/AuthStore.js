// import { makeAutoObservable, runInAction, reaction } from "mobx"

import { makeObservable, observable, computed } from "mobx"


class AuthStore {
    data = null


    constructor() {
        makeObservable(this, {
            data: observable,
            expNeeded: computed
            // userLvl: computed
        })
    }

    get expNeeded() {
        if (this.data) {
            if (this.data.level === 0) {
                return 10
            }
            else {return (10 + this.data.level * 10 * (this.data.level - 1))}
        }
        else {return console.log("oa")}
    }

    // get userLvl() {
    //     if (this.data) {
    //         let increment = 10
    //         let lvl = Math.floor((1/40)*this.data.experience + (3/4))
    //         this.expNeeded = ((lvl + 1)- (3/4))/(1/40)
    //         return lvl

    //     } else {
    //         return "Не удалось получить ваш уровень"
    //     }   
    // }

    selectIsAuth = () => {
        return Boolean(this.data)
    }
    // selectIsAdmin = () => {
    //     if (this.data.role !== null) {
    //         return this.data.role
    //     } else {
    //         return false
    //     }
    // }

    logout = () => {
        this.data = null
    }


}

export const authStore = new AuthStore()

