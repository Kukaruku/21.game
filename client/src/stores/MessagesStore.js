import { makeObservable, observable } from "mobx"
import {makePersistable} from "mobx-persist-store"

class MessagesStore {
    messages = []
    author = ""
    textMessages = []

    constructor() {
        makeObservable(this, {
            messages: observable,
            textMessages: observable
        })
        makePersistable(this, {
            name: "localMessages",
            properties: ["messages"],
            storage: window.localStorage,
            expireIn: 86400000
        })
    }


    

    // messagePush = (text) => {
    //     this.messages.push({author: this.author, text: text})
    // }
}

export const messagesStore = new MessagesStore()