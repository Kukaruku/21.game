import { computed, makeObservable, observable } from "mobx"

class GameStore {
    gameStarted = false
    inGame = null
    gameId = null
    player = null
    opp = null
    cards = [1,1,1,1,4,4,4,4,3,3,3,3,2,2,2,2,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
    cards_out = []
    player_cards = []
    opp_cards = []
    opp_revealed_cards = []
    player_rounds_wins = null
    opp_rounds_wins = null
    top_card = null
    playerTurn = null
    addTurn = null
    turnsCount = 0
    wins = [false, false, false]
    opp_wins = [false, false, false]
    state = ""
    end = ""

    constructor() {
        makeObservable(this, {
            // cards: observable,
            opp: observable,
            wins: observable,
            opp_wins: observable,
            inGame: observable,
            cards_out: observable,
            player_cards: observable,
            opp_cards: observable,
            opp_revealed_cards: observable,
            top_card: observable,
            player: observable,
            addTurn: observable,
            playerTurn: observable,
            turnsCount: observable,
            state: observable,
            oppScore: computed,
            cycled: computed,
            playerScore: computed,
        })
    }


    getTopCard(card = null) {
        if (!card) {
            const r_nmb = Math.floor(Math.random() * (this.cards.length))
            const r_card = this.cards[r_nmb]
            this.cards.splice(r_nmb, 1)
            this.cards_out.push(r_card)
            this.top_card = r_card
            // return r_card
        } else {
            // this.cards.splice(r_nmb, 1)
            this.cards_out.push(card)
            this.top_card = card
            // return r_card
        }
    }

    get cycled () {
        return this.turnsCount === 2
    }

    playerMove() {
        this.player_cards.push(this.top_card)
        this.getTopCard()
    }
    oppMove(card) {
        this.opp_cards.push(this.top_card)
        this.getTopCard(card)
    }

    resetCards() {
        this.cards = [1,1,1,1,4,4,4,4,3,3,3,3,2,2,2,2,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
        this.cards_out = []
    }

    endTurn() {
        this.playerTurn = !this.playerTurn
        this.state = "Ходит противник"
    }

    turnStart() {
        this.getTopCard()
        this.state = "ВЫ ходите первым"
    }

    aceChange(index) {
        if (this.opp_revealed_cards.length < index + 1) {
            // gameStore.player_cards[index] === 1 ? gameStore.player_cards[index] = 11 : gameStore.player_cards[index] = 1
            let aceIndex = index - this.opp_revealed_cards.length
            gameStore.opp_cards[aceIndex] === 1 ? gameStore.opp_cards[aceIndex] = 11 : gameStore.opp_cards[aceIndex] = 1
        }
        else {
            gameStore.opp_revealed_cards[index] === 1 ? gameStore.opp_revealed_cards[index] = 11 : gameStore.opp_revealed_cards[index] = 1
            return index
        }
    }


    addWin(score) {
        console.log("doing somthing")
        let arr = []
        let newWin = true
        score.forEach(element => {
            console.log("iteration")
            if (newWin === true) {
                console.log("new")
                console.log(element)
                if (element === false) {
                    console.log("yes")
                    arr.push(!element)
                    newWin = false
                    console.log(arr) 
                } else {
                    console.log("no")
                    arr.push(true)
                }
            } else {
                console.log("old")
                arr.push(false)
                console.log(arr) 
            }
        })
        score.forEach(element => console.log("dadada"))
        console.log(arr) 
        return arr
    }


    scoreCheck() {
        let oppScore = this.oppScore
        let playerScore = this.playerScore

        if ((oppScore >= 21) || (playerScore >= 21)) {
            if (oppScore === 21) {
                if (playerScore === 21) {gameStore.end = "Ничья"; return "Ничья"}
                if (playerScore !== 21) {this.opp_wins = this.addWin(this.opp_wins); gameStore.end = "В этом раунде победил противник"; return "В этом раунде победил противник"}
            }
            if (playerScore === 21) {
                if (oppScore === 21)  {gameStore.end = "ничья"; return "Ничья"}
                if (oppScore !== 21) {this.wins = this.addWin(this.wins); gameStore.end = "В этом раунде победили вы"; return "В этом раунде победили вы"}
            }
            if (oppScore > 21) {
                if (playerScore === 21) {this.wins = this.addWin(this.wins); gameStore.end = "В этом раунде победили вы"; return "В этом раунде победил вы"}
                else {
                    let oppNmb = Math.abs(21 - oppScore)
                    let playerNmb = Math.abs(21 - playerScore)
                    if (oppNmb > playerNmb) {
                        {this.wins = this.addWin(this.wins);  gameStore.end = "В этом раунде победили вы"; return "В этом раунде победили вы"}
                    } if (oppNmb < playerNmb) {this.opp_wins = this.addWin(this.opp_wins); gameStore.end =  "В этом раунде победил противник"; return "В этом раунде победил противник"}
                    else {gameStore.end = "Ничья"; return "Ничья"}
                }
            }
            if (playerScore > 21) {
                if (oppScore === 21) {this.opp_wins = this.addWin(this.opp_wins); gameStore.end =  "В этом раунде победил противник"; return "В этом раунде победил противник"}
                else {
                    let oppNmb = Math.abs(21 - oppScore)
                    let playerNmb = Math.abs(21 - playerScore)
                    if (oppNmb > playerNmb) {
                        {this.wins = this.addWin(this.wins);  gameStore.end =  "В этом раунде победили вы"; return "В этом раунде победили вы"}
                    } if (oppNmb < playerNmb) {this.opp_wins = this.addWin(this.opp_wins);gameStore.end =  "В этом раунде победил противник"; return "В этом раунде победил противник"}
                    else {gameStore.end = "Ничья"; return "Ничья"}
                }
            }
        } else {return true}
    }

    roundEnd() {
        this.cards = [1,1,1,1,4,4,4,4,3,3,3,3,2,2,2,2,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
        this.cards_out = []
        this.player_cards = []
        this.opp_cards = []
        this.opp_revealed_cards = []
        this.top_card = null
    }

    gameEnd() {
        this.gameStarted = false
        this.inGame = null
        this.gameId = null
        this.player = null
        this.opp = null
        this.cards = [1,1,1,1,4,4,4,4,3,3,3,3,2,2,2,2,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
        this.cards_out = []
        this.player_cards = []
        this.opp_cards = []
        this.opp_revealed_cards = []
        this.player_rounds_wins = null
        this.opp_rounds_wins = null
        this.top_card = null
        this.playerTurn = null
        this.addTurn = null
        this.turnsCount = 0
        this.wins = [false, false, false]
        this.opp_wins = [true, false, false]
    }

    get oppScore() {
        if (!this.opp_revealed_cards.length) {
            return 0
        } else {return this.opp_revealed_cards.reduce((sum, elem) => {return sum + elem})}
        
    }
    get playerScore() {
        if (!this.player_cards.length) {
            return 0
        } else {return this.player_cards.reduce((sum, elem) => {return sum + elem})}
    }
}

export const gameStore = new GameStore()