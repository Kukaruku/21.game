import Main from "./components/pages/main/Main";
import Auth from "./components/pages/Auth/Auth";
import Profile from "./components/pages/Profile/Profile";
import "./App.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navigate, Route, Routes } from "react-router-dom"
import Search from "./components/pages/Search/Search";
import GameMp from "./components/pages/GameMp/GameMp";
import { useEffect} from "react";
import { authStore } from "./stores/AuthStore";
import { fetchUsersApi } from "./API";
import Register from "./components/pages/Register/Register";
import { observer } from "mobx-react";
import { gameStore } from "./stores/GameStore";
import About from "./components/pages/About/About";


const App = observer(() => {
  // const isAuth = authStore.selectIsAuth()
  

  useEffect(() => {
    async function fetchData() {
      authStore.data = await fetchUsersApi.fetchAuthMe()
    }
    fetchData()


  }, [])

  return (
    gameStore.inGame ? 
    <Routes>
      <Route path="/aa" element={<Main/>}/>
      <Route path="/auth" element={<Auth/>}/>
      <Route path="/reg" element={<Register/>}/>
      <Route path="/profile" element={<Profile/>}/>
      <Route path="/" element={<About/>}/>
      <Route path="/search" element={<Search/>}/>
      <Route path="/gameMp/:id" element={<GameMp/>}/>
      <Route path='*' element={<Navigate to="/"/>} />
    </Routes>
    :
    <Routes>
      <Route path="/aa" element={<Main/>}/>
      <Route path="/auth" element={<Auth/>}/>
      <Route path="/reg" element={<Register/>}/>
      <Route path="/profile" element={<Profile/>}/>
      <Route path="/" element={<About/>}/>
      <Route path="/search" element={<Search/>}/>
      <Route path='*' element={<Navigate to="/"/>} />
    </Routes>
  );
})

export default App;
