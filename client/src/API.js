import axios from "./axios.js"

export const fetchUsersApi = {
    fetchUserData: async (params) =>  {
        const { data } = await axios.post("/auth", params)
            .catch((error) => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    return error.response.data
                }
            })
        return data
    },
    fetchRegister: async (params) =>  {
        const { data } = await axios.post("/register", params)
            .catch((error) => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    return error.response.data
                }
            })
        return data
    },

    fetchAuthMe: async () =>  {
        const { data } = await axios.get("/profile")
            .catch((error) => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    return error.response.data
                }
            })
        return data
    },


    // fetchUserData: async (params) =>  {
    //     await axios.post("/auth", params)
    //         .then((res) => {
    //             console.log(res.data)
    //         })
    //         .catch((error) => {
    //             if (error.response) {
    //                 console.log(error.response.data);
    //                 console.log(error.response.status);
    //                 return error.response.data
    //             }

    //     })
    // }
}

