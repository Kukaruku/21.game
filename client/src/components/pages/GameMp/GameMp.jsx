import Chat from "../../UI/Chat/Chat"
import Sidebar from "../../UI/Sidebar/Sidebar"
import classes from "./GameMp.module.css"
import img from "../../imgs/base_87716f252d.jpg"
import Button from 'react-bootstrap/Button';
import aceImg from "../../imgs/spades-card-svgrepo-com.svg"
import { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { gameStore } from "../../../stores/GameStore";
import { socket } from "../../../socket";
import { reaction, toJS } from "mobx";
import { socket2 } from '../../../socket';
import { authStore } from "../../../stores/AuthStore";

const AceSvg = ({active}) => {
    const aceClasses = [classes.ace_svg]
    active && aceClasses.push(classes.ace_active)
    
    return (
    <svg
      className={aceClasses.join(" ")}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="-1.6 -1.6 35.2 35.2"
    >
      <g
        stroke="#eba6f2"
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <title>{"spades-card"}</title>
        <path d="m28.565 7.822-11.588-6.69a3.156 3.156 0 0 0-4.31 1.155L2.41 20.052a3.156 3.156 0 0 0 1.155 4.31l11.588 6.69a3.156 3.156 0 0 0 4.31-1.155L29.72 12.132a3.156 3.156 0 0 0-1.155-4.31zM14.771 3.77a1.382 1.382 0 1 1 2.394 1.382A1.382 1.382 0 0 1 14.77 3.77zm2.52 24.763a1.382 1.382 0 1 1-2.394-1.382 1.382 1.382 0 0 1 2.394 1.382zm3.618-9.713v0c-1.599 2.58-4.947 1.725-5.387-1.059l-.202 4.947-4.643-2.681 4.055-2.567c-2.548.797-5.041-1.637-3.451-4.199 1.817-2.926 7.087-2.201 8.535-3.65-.485 1.811 2.832 6.403 1.093 9.208z" />
      </g>
      <title>{"spades-card"}</title>
      <path d="m28.565 7.822-11.588-6.69a3.156 3.156 0 0 0-4.31 1.155L2.41 20.052a3.156 3.156 0 0 0 1.155 4.31l11.588 6.69a3.156 3.156 0 0 0 4.31-1.155L29.72 12.132a3.156 3.156 0 0 0-1.155-4.31zM14.771 3.77a1.382 1.382 0 1 1 2.394 1.382A1.382 1.382 0 0 1 14.77 3.77zm2.52 24.763a1.382 1.382 0 1 1-2.394-1.382 1.382 1.382 0 0 1 2.394 1.382zm3.618-9.713c-1.599 2.58-4.947 1.725-5.387-1.059l-.202 4.947-4.643-2.681 4.055-2.567c-2.548.797-5.041-1.637-3.451-4.199 1.817-2.926 7.087-2.201 8.535-3.65-.485 1.811 2.832 6.403 1.093 9.208z" />
    </svg>)
}


const PlayerCardOpp = observer(({avatarUrl}) => {

    useEffect(() => {
        if (!gameStore.opp) {
            socket2.emit("userData", authStore.data)
        }

        const socketUserData = (data) => {
            gameStore.opp = {username: data.name, lvl: data.level}
        }

        socket2.on("userDataOff", socketUserData)

        return () => {
            socket2.off("userDataOff", socketUserData)
        }

    }, [])

    return (
        <div className={classes.player_card_cont}>
            <div className={classes.card_avatar} style={{backgroundImage: `url(${avatarUrl})`}}/>
            <div className={classes.card_info}>
                <div className={classes.card_title}>Ваш противник</div>
                <div className={classes.card_username}>{gameStore.opp ? gameStore.opp.username : "???"}</div>
                {/* <div className={classes.card_username}>name</div> */}
                <div className={classes.card_level}>Уровень: {gameStore.opp ? gameStore.opp.lvl : "???"}</div>
            </div>
        </div>
    )
})

const PlayerCardYou = ({avatarUrl}) => {
    return (
        <div className={classes.player_card_cont}>
            <div className={classes.card_avatar} style={{backgroundImage: `url(${avatarUrl})`}}/>
            <div className={classes.card_info}>
                <div className={classes.card_title}>Вы</div>
                <div className={classes.card_username}>{authStore.data.name}</div>
                {/* <div className={classes.card_username}>name</div> */}
                <div className={classes.card_level}>Уровень: {authStore.data.level}</div>
            </div>
        </div>
    )
}

const Card = ({opp, active, nmb, index, back, ace, ...props}) => {



    const [aceActive, setAceActive] = useState(false)

    useEffect(() => {if ((gameStore.opp_revealed_cards[index] === 11) && opp) {
        setAceActive(true)
    } else if (opp) {
        setAceActive(false)
        }}
    )

    const handleAceClick = () => {
        gameStore.player_cards[index] === 1 ? gameStore.player_cards[index] = 11 : gameStore.player_cards[index] = 1  
        socket2.emit("aceChange", index, gameStore.gameId)
        aceActive ? setAceActive(false) : setAceActive(true)
    }

    return (
        back 
        ?
        <div className={`${classes.card_back} `} {...props}/>
        : ace ? 
        <div className={`${classes.card_front} ${classes.card_front_ace} ${classes.card_opp}`} onClick={()=>{handleAceClick()}} {...props}>
            <div className={`${classes.number} ${classes.top} ${classes.ace_small}`}><img src={aceImg} alt="" className={classes.ace_img_small}/></div>
            {/* <div className={classes.ace}><img src={aceImg} alt="" className={classes.ace_img}/></div> */}
            {/* <div className={classes.ace}><div className={aceClasses.join(" ")}/></div> */}
            <div className={classes.ace}><AceSvg active={aceActive}/></div>
            <div className={`${classes.number} ${classes.ace_small} ${classes.ace_bottom}`}><img src={aceImg} alt="" className={`${classes.ace_img_small}`}/></div>
        </div>
        :
        <div className={`${classes.card_front} `} {...props}>
            <div className={`${classes.number} ${classes.top}`}>{nmb}</div>
            <div className={classes.logo}>21.game</div>
            <div className={`${classes.number} ${classes.bottom}`}>{nmb}</div>
        </div>
    )
}

const GameMp = observer(() => {


    const handleAddClick = () => {
        gameStore.playerMove()
        gameStore.addTurn = false;
        socket2.emit("playerTurn", gameStore.top_card, gameStore.player_cards[gameStore.player_cards.length - 1])
        console.log("dobavit " + gameStore.turnsCount)
    }

    const handleEndTurnClick = () => {
        socket2.emit("step")
        gameStore.playerTurn = false;
        gameStore.turnsCount++
        console.log("End turn click turnCount " + gameStore.turnsCount)
    }

    const onsocket = (top_card, opp_card) => { 
        gameStore.top_card = top_card
        gameStore.opp_cards.push(opp_card)
    }



    const gameEnd = () => {
        gameStore.gameEnd()
        console.log("Игра закончилась")
        socket2.disconnect()
    }

    useEffect(() => {

        const step = () => {
            console.log("PRISHLO")
            gameStore.addTurn = true
            gameStore.playerTurn = true
            gameStore.turnsCount++
        }

        const aceChange = (index) => {
            gameStore.aceChange(index)
        }

        reaction(
            () => gameStore.cycled,
            () => {
                if (gameStore.cycled === true) 
                {
                    if (gameStore.opp_cards.length > 0) {
                        gameStore.opp_revealed_cards.push.apply(gameStore.opp_revealed_cards, gameStore.opp_cards)
                    }
                    gameStore.opp_cards = []
                    let contRound = gameStore.scoreCheck()
                    if (contRound != true) {
                        console.log("RRRRRRAUND")
                        gameStore.roundEnd()
                        gameStore.turnStart()
                        console.log("игра завершена")
                        if ((gameStore.wins[2] === true) || (gameStore.opp_wins[2] === true)) {
                            alert(gameStore.end)
                            socket2.disconnect()
                            gameStore.gameEnd()
                        }
                    }
                    gameStore.turnsCount = 0
                }
            }
        )

        socket2.on("oppTurn", onsocket)
        socket2.on("exit", gameEnd)
        socket2.on("step", step)
        socket2.on("aceChange", aceChange)
        return () => {
            socket2.off("oppTurn", onsocket)
            socket2.off("exit", gameEnd)
            socket2.off("step", step)
            socket2.off("aceChange", aceChange)
        }
    })



    reaction(
        () => gameStore.playerScore,
        () => {
            if (gameStore.playerScore >= 21) {

            }
        }
    )



    return (
        <>
        <Sidebar/>
        <Chat/>
        <div className={classes.main_cont}>
            <PlayerCardOpp avatarUrl={img}/>
            <div className={classes.game_cont}>
                <div className={classes.card_container}>
                    <div className={classes.lampochka_cont}>
                        {gameStore.opp_wins.map((card, index) => {
                            if (card) {
                                return <div className={`${classes.lampochka} ${classes.green}`}/>
                            } else {
                                return <div className={classes.lampochka}/>
                            }
                            })
                        }
                    </div>
                    <Card nmb={gameStore.top_card} back/>
                    <div className={classes.lampochka_cont}>
                        {gameStore.wins.map((card, index) => {
                            if (card) {
                                return <div className={`${classes.lampochka} ${classes.green}`}/>
                            } else {
                                return <div className={classes.lampochka}/>
                            }
                            })
                        }
                    </div>
                </div>
                <div className={classes.game_main_container}>
                    {/* <div className={classes.logs}>Ходит яяя</div> */}
                    <div className={classes.opp_card_container}>
                        <div>Карты противника</div>
                        <div className={classes.mask}/>
                        <div className={`${classes.cards} ${classes.cards_opp}`}>
                            {gameStore.opp_revealed_cards.map((card, index) => 
                                {if ((card === 1) || (card === 11)) {
                                    return <Card opp ace disabled key={index} index={index} style={{width: 80, height: 120}} onClick={()=>{}}/>
                                }
                                return <Card nmb={card} key={index} style={{width: 80, height: 120}}/>}
                            )}
                            {gameStore.opp_cards.map((card, index) => 
                                {if ((card === 1) || (card === 11)) {
                                    return <Card back ace key={index} index={index} style={{width: 80, height: 120}}/>
                                }
                                return <Card back nmb={card} key={index} style={{width: 80, height: 120}}/>}
                            )}

                        </div>
                        <div>Общий счет противника: {gameStore.oppScore}</div>
                    </div>
                    <div className={classes.player_card_container}>
                        <div>Ваши карты</div>
                        <div className={classes.cards}>
                            {gameStore.player_cards.map((card, index) => 
                                {if ((card === 1) || (card === 11)) {
                                    return <Card ace key={index} index={index}/>
                                }
                                return <Card nmb={card} key={index}/>}
                            )}
                        </div>
                        <div>Ваш общий счет: {gameStore.playerScore}</div>
                    </div>
                    <div className={classes.buttons_container}>
                        <Button disabled={!gameStore.addTurn} className={classes.button_bottom} onClick={()=>handleAddClick()}>Добавить</Button>
                        {/* <Button className={classes.button_bottom}>Название навыка?</Button> */}
                    </div>
                </div>
                <div className={classes.timer_container}>
                    {/* <div className={classes.timer}>10:00</div> */}
                    <Button disabled={!gameStore.playerTurn} variant="danger" className={classes.end_turn_button} onClick={() => handleEndTurnClick()}>Закончить ход</Button>
                </div>
            </div>
            <PlayerCardYou/>
        </div>
        </>
    )
})

export default GameMp