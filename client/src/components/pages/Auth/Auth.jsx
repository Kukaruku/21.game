import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import classes from "./Auth.module.css"
import React from "react";
import { Link, Navigate } from 'react-router-dom';
import {useForm} from "react-hook-form"
import { fetchUsersApi } from '../../../API';
import { authStore } from '../../../stores/AuthStore';

const Auth = () => {
    const isAuth = authStore.selectIsAuth()
    
    const { register, handleSubmit, formState: {errors}} = useForm({
        defaultValues: {
            email: "aboba228",
            password: "12345"
        }
    })

    const onSubmit = async (values) => {
        authStore.data = await fetchUsersApi.fetchUserData(values)


        if (!authStore.data) {
            return alert("Не удалось авторизоаваться")
        }

        if ("token" in authStore.data) {
            window.localStorage.setItem("token", authStore.data.token)
        }
    }

    if (isAuth) {
        return <Navigate to="/"/>
    }

    return (
        <Form className={classes.form_box} onSubmit={handleSubmit(onSubmit)}>
            <Form.Group>
                <h1 className={classes.title}>Вход</h1>
                <Form.Group>
                    <Form.Label>Почта</Form.Label>
                    <Form.Control type="email" {...register("email", {required: "Укажите почту"})} />
                    <Form.Label className={classes.error_message}>{errors.email?.message}</Form.Label>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control type="password" {...register("password", {required: "Укажите пароль"})}/>
                    <Form.Label className={classes.error_message}>{errors.password?.message}</Form.Label>
                </Form.Group>
                <Form.Group className={classes.check} controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Запомнить меня" />
                </Form.Group>
                <div className="d-grid gap-2">
                    <Button variant="primary" type="submit">
                        Войти
                    </Button>
                </div>
                <div className={classes.noakk_cont}>
                    <Form.Label>Нет аккаунта?</Form.Label>
                    <Form.Label className={classes.text}><Link to={"/reg"}>Зарегистрироваться</Link></Form.Label>
                </div>
            </Form.Group>
        </Form>
    )
}

export default Auth;
