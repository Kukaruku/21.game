import { useEffect, useState } from "react";
import Sidebar from "../../UI/Sidebar/Sidebar"
import classes from "./Profile.module.css"
import ProgressBar from 'react-bootstrap/ProgressBar';
import { authStore } from "../../../stores/AuthStore";
import { observer } from "mobx-react";
import Chat from "../../UI/Chat/Chat";
// import axios from "../../../axios"



const Perks = ({userLvl}) => {
    const [activeP, setActiveP] = useState(null)

    const click = (value) => {
        setActiveP(value)
    }

    useEffect(() => {
    }, [])

    return (
        <div className={classes.perks_grid}>
            <Perk actives={activeP} index={1} onclick={click} lvl={userLvl}/>
            <Perk actives={activeP} index={2} onclick={click} lvl={userLvl}/>
            <Perk actives={activeP} index={3} onclick={click} lvl={userLvl}/>
            <Perk actives={activeP} index={4} onclick={click} lvl={userLvl}/>
            <Perk actives={activeP} index={5} onclick={click} lvl={userLvl}/>
        </div>
    )
}

const Perk = ({actives, index, onclick, lvl, bg}) => {

    const HoverModal = ({locked, coordX, coordY}) => {
        return (
            // <div className={classes.modal_cont} style={{transform: `translate(${coordX - 610}px, ${coordY - 360}px)`}}>
            <div className={classes.modal_cont}>
                <div className={classes.perk_title}>ПЛЮС АДИН</div>
                <div>Добавляет к вашему счету одно очко</div>
                {locked ? 
                <div className={classes.lvl_req}>&#128274; Требуемый уровень: {index}</div>
                :
                <div className={classes.lvl_req}>Открыто</div>}
            </div>
        )
    }

    const [hover, setHover] = useState(null)

    // const handleHover = (element, locked=false) => {
    //     setHover(<HoverModal locked={locked} coordX={element.clientX} coordY={element.clientY}/>)
    // }

    const handleHover = (locked=false) => {
        setHover(<HoverModal locked={locked} />)
    }

    const handleLeave = () => {
        setHover(null)
    }

    const callback = () => {
        onclick(index)
    }


    if (lvl >= index) {
        return (actives === index ? 
            <div className={classes.perk_wrap}>
                <div className={`${classes.perk_cont} ${classes.active}`} onMouseMove={() => handleHover()} onMouseLeave={() => handleLeave()}>
                    <div className={classes.perk_img} style={{backgroundImage: `url(${bg})`}} />
                </div>
                {hover}
            </div>
            : <div className={classes.perk_wrap}>
                <div className={classes.perk_cont} onClick={() => callback(index)} onMouseMove={() => handleHover()} onMouseLeave={() => handleLeave()}>
                    <div className={classes.perk_img} style={{backgroundImage: `url(${bg})`}} />
                </div>
                {hover}
            </div>)
        }
    else {
        return (
            <div className={classes.perk_wrap}>
                <div className={`${classes.perk_cont} ${classes.locked}`} onMouseMove={() => handleHover(true)} onMouseLeave={() => handleLeave()}>
                    <div className={classes.perk_img}/>
                </div>
                {hover}
            </div>
        )} 
}

const Profile = observer(() => {

    const userData = authStore.data
    const expNeeded = authStore.expNeeded

    const barProgress = () => {
        return userData.experience * 100 / expNeeded
    }

    try {return (
        <>
        <Chat/>
        <Sidebar key={1}/>
        <div className={classes.prof_main_cont}>
            <div className={classes.profile_card}>
                <div className={classes.avatar}/>
                <div className={classes.profile_info}>
                    <div className={classes.user_cont}>
                        <div className={classes.username} style={{color: userData.color}}>{userData.name}</div>
                        {/* {authStore.data.role ? <div className={classes.email}>admin</div> : <div className={classes.email}>admin</div>} */}
                    </div>

                    <div className={classes.email}>EMAIL: {userData.email}</div>
                    <div className={classes.email}>LEVEL: {userData.level}</div>
                    <div className={classes.re}>
                        <div className={classes.email}>Сменить почту</div>
                        <div className={classes.email}>Сменить пароль</div>
                    </div>
                </div>
            </div>
            <div className={classes.lvl_wrap}>
                <div className={classes.lvl_count}>{userData.level + " lvl"}</div>
                <div className={classes.exp_count}>{userData.experience + "/" + authStore.expNeeded}</div>
            </div>
            <ProgressBar className={classes.prog_bar} animated now={barProgress()} />
            {/* <div className={classes.next_perk_wrap}>
                <div className={classes.next_perk_title}>След. навык:</div>
                <div className={classes.next_perk}>blabla</div>
            </div> */}
            <div className={classes.your_perks_title}>Ваши навыки</div>
            <Perks userLvl={userData.level}/>
            <div className={classes.your_perks_title}>История игр?</div>
        </div>
        </>
    )}
    catch {
        return (
            <>
            <Sidebar key={1}/>
            <div className={classes.prof_main_cont}>Загрузка...</div>
            </>
        )
    }
})

export default Profile