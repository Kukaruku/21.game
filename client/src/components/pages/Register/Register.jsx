import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import classes from "./Register.module.css"
import React from "react";
import { Navigate, useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { authStore } from '../../../stores/AuthStore';
import { fetchUsersApi } from '../../../API';
import Background from '../../imgs/base_87716f252d.jpg';
import { observer } from 'mobx-react';


const Register = observer(() => {
    const isAuth = authStore.selectIsAuth()
    const navigate = useNavigate()
    
    const { register, handleSubmit, formState: {errors, isValid}} = useForm({
        defaultValues: {
            name: "Warhammer",
            email: "bebrius@mail.ru",
            password: "12345",
            passwordRepeat: "12345"
        }
    })

    const onSubmit = async (values) => {
        const data = await fetchUsersApi.fetchRegister(values)
        console.log(data)


        if (!data) {
            return alert("Не удалось зарегистрироваться")
        } else if (data?.sucsess === false) {
            return alert(data.message)
        }

        if ("token" in data) {
            window.localStorage.setItem("token", data.token)
            window.location.reload()
        }

        if (isAuth) {
            navigate(`/`)
        }
    }

    if (isAuth) {
        navigate(`/`)
    }


    return (
        <Form className={classes.form_box} onSubmit={handleSubmit(onSubmit)}>
            <Form.Group className={classes.form_wrap}>
                <h1 className={classes.title}>Регистрация</h1>
                <div className={classes.avatar} style={{backgroundImage: `url(${Background})`}}>
                    <div className={classes.avatar_hover}>
                        <div className={classes.photo_img}>

                        </div>
                    </div>
                </div>
                <Form.Group>
                    <Form.Label>Почта</Form.Label>
                    <Form.Control type="email" {...register("email", {required: "Укажите почту"})}/>
                    <Form.Label className={classes.error_message}>{errors.email?.message}</Form.Label>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Никнейм</Form.Label>
                    <Form.Control type="text" {...register("name", {required: "Укажите никнейм"})}/>
                    <Form.Label className={classes.error_message}>{errors.username?.message}</Form.Label>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control type="password" {...register("password", {required: "Укажите пароль"})}/>
                    <Form.Label className={classes.error_message}>{errors.password?.message}</Form.Label>
                </Form.Group>
                {/* <Form.Group>
                    <Form.Label className={classes.repeat_pass}>Повторите пароль</Form.Label>
                    <Form.Control type="password" {...register("passwordRepeat", {required: "Повторите пароль"})}/>
                    <Form.Label className={classes.error_message}>{errors.passwordRepeat?.message}</Form.Label>
                </Form.Group> */}
                <div className="d-grid gap-2">
                    <Button disabled={!isValid} variant="primary" type="submit">
                        Зарегистрироваться
                    </Button>
                </div>
            </Form.Group>
        </Form>
    )
})

export default Register