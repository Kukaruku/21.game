export default class game21 {
    cards = [1,1,1,1,4,4,4,4,3,3,3,3,2,2,2,2,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
    cards_out = []

    getCard(min, max) {
        const r_nmb = Math.floor(Math.random() * (max - min + 1) + min)
        const r_card = this.cards[r_nmb]
        this.cards.splice(r_nmb, 1)
        this.cards_out.push(r_card)
        return r_card
    }

    resetCards() {
        this.cards = [1,1,1,1,4,4,4,4,3,3,3,3,2,2,2,2,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10]
        this.cards_out = []
    }
}