import classes from "./Main.module.css"
import Button from 'react-bootstrap/Button';
import Game21 from "./21_class.js"
import React, { useState } from "react";
import { makeAutoObservable } from "mobx"
import { observer } from "mobx-react";
import Sidebar from "../../UI/Sidebar/Sidebar";
import Chat from "../../UI/Chat/Chat";

const game = new Game21()



const Card = ({nmb, index, setAceCards, aceCards}) => {
    const [aceNmb, setAceNmb] = useState(1)

    const setClickCards = () => {
        let arr = aceCards;
        if (arr[index] === 1 ) {
            setAceNmb(11)
            arr[index] = 11
            setAceCards(arr)
            fstSum.increaseSum(10)
        } else {
            setAceNmb(1)
            arr[index] = 1
            setAceCards(arr)
            fstSum.decreaseSum(10)
        }
    }

    if (nmb !== 11 && nmb !== 1) {
        return (
            <div className={classes.card_container}>
                <div className={classes.number_top}>{nmb}</div>
                <div className={classes.number_bot}>{nmb}</div>
            </div>
        )
    } else {
        return (
            <div className={classes.card_container}>
                <div className={classes.number_top}>{aceNmb}</div>
                <div className={classes.number_mid} onClick={() => setClickCards()}>ace</div>
                <div className={classes.number_bot}>{aceNmb}</div>
            </div>
        )
    }
}

// sum //////////////////////////////

class PointsSum {
    sum = 0

    constructor() {
        makeAutoObservable(this)
    }

    increaseSum(value) {
        this.sum += value
    }
    decreaseSum(value) {
        this.sum -= value
    }
}

const fstSum = new PointsSum()

const PointSum = observer(({points}) => {
    return <div>Сумма: {points.sum}</div>
})

// end sum /////////////////////////////

const Main = () => {
    const [cards, setCards] = useState([])

    const change = (cards) => {
        setCards(cards)
    }

    const addCard = () => {
        const new_card = game.getCard(0, game.cards.length-1);
        setCards([...cards, new_card])
        fstSum.increaseSum(new_card)
    }

    const reset = () => {
        game.resetCards()
        setCards([])
        fstSum.sum = 0
    }

    // React.useLayoutEffect(() => {
    //     for (let i = 0; i < cards.length; i++) {
    //         if (cards[i] !== "ace") {
    //             fstSum.sum += cards[i];
    //         } else {fstSum.sum += 1}
    //     }
    //     setCardsSum(fstSum.sum)
    // }, [cards])


    return (
        <>
        <Sidebar/>
        <Chat/>
      <div className={classes.main_container}>
        <div className={classes.title}>Игра в одиночку</div>
        <div className={classes.cards_remaining}>
            <div>Карт</div>
            <div>осталось:</div>
            <div>{game.cards.length}</div>
        </div>
        <div className={classes.cards_grid}>
            <div>Ваши карты:</div>
            <div className={classes.cards}>
                {cards.length === 0 ? <div className={classes.cards_null} /> : 
                cards?.map((card, index) =>
                {if (card !== 11 && card !== 1) {
                    return <Card nmb={card} key={index}/>
                } else { return <Card nmb={card} index={index} key={index} setAceCards={change} aceCards={cards} /> }} 
            )}
            </div>
            <PointSum points={fstSum} />
        </div>
        <div className={classes.btn_cont}>
            <Button variant="danger" onClick={()=>reset()}>Закончить</Button>
            <Button variant="success" onClick={()=>addCard()}>Добавить</Button>
            <Button variant="primary" >Использовать навык</Button>
            <div>Навык</div>
        </div>
      </div>
      </>
    );
  }
  
export default Main;