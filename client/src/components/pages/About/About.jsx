import Chat from "../../UI/Chat/Chat"
import Sidebar from "../../UI/Sidebar/Sidebar"
import classes from "./About.module.css"

const About =() => {
    return (
        <>
        <Sidebar/>
        <Chat/>
        <div className={classes.prof_main_cont}>
        <div className={classes.main_cont}>
            <h1>История</h1>
            <div>
                Где зародилась игра “Очко” или “21”(двадцать одно) точно уже установиться не удастся. Существует два варианта возникновения игры. Первый вариант – игра произошла от “Блэкджека”, но в связи с тем, что у нас были более распространены колоды из 36 карт, игра немного изменилась. По второй версии игра произошла от французской игры “Vingt-et-un”, что в переводе означает “21”.</div>
            </div>
        </div>
        </>
    )
}

export default About