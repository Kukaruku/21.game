import Container from 'react-bootstrap/Container';
import Spinner from 'react-bootstrap/Spinner';
import Button from 'react-bootstrap/Button';
import classes from "./Search.module.css"
import Sidebar from '../../UI/Sidebar/Sidebar';
import { Link, Navigate, useNavigate } from "react-router-dom"
import Chat from '../../UI/Chat/Chat';
import { socket2 } from '../../../socket';
import { useEffect } from 'react';
import { authStore } from '../../../stores/AuthStore';
import { gameStore } from '../../../stores/GameStore';

const Search = () => {

    const navigate = useNavigate()
    // const socket2 = io("http://localhost:4001");

    const handleCancel = () => {
        socket2.emit("userIdOff", socket2.id)
        socket2.disconnect()
    }

    useEffect(() => {
        const onConnect = () => {
            // socket2.emit("userIdOn", authStore.data.id)
            socket2.emit("start", socket2.id)
            console.log("connected")
        }
        const onDisconnect = () => {
            socket2.emit("userIdOff", socket2.id)
            console.log("disconnected")
        }

        const ready = (roomId) => {
            gameStore.inGame = true
            gameStore.gameId = roomId
            navigate(`/gameMp/${roomId}`)
        }

        const start = (bool) => {
            gameStore.playerTurn = bool
            gameStore.addTurn = bool
            gameStore.getTopCard()
            console.log("игра началась!")
            console.log("игра началась!T " + gameStore.turnsCount)
        }

        socket2.on("connect", onConnect)
        socket2.on("disconnect", onDisconnect)
        socket2.on("ready", ready)
        socket2.on("firstTurn", start)


        
        return () => {
            socket2.off('connect', onConnect);
            socket2.off('disconnect', onDisconnect);
            socket2.off("ready", ready)
            socket2.off("firstTurn", start)

        }

    }, [])


    return (
        <>
        <Sidebar key={1}/>
        <Chat/>
        <Container className={classes.search_cont}>
            <Spinner animation="border" role="status"/>
            <div>Идет поиск противника...</div>
            <Link to="/" className={classes.cancel_link}>
                <Button variant="danger" className={classes.search_button} onClick={() => handleCancel()} >Отмена</Button>
            </Link>
        </Container>
        </>
    )
}

export default Search