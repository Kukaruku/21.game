import React, { useState } from "react"
import classes from "./Sidebar.module.css"
import profImg from "../../imgs/profile-round-1342-svgrepo-com.svg"
import logoutImg from "../../imgs/logout-svgrepo-com.svg"
import loginImg from "../../imgs/login-3-svgrepo-com.svg"
import aboutImg from "../../imgs/question-square-svgrepo-com.svg"
import profileImg from "../../imgs/base_87716f252d.jpg"
import searchImg from "../../imgs/search-svgrepo-com.svg"
import homeImg from "../../imgs/home-svgrepo-com.svg"
import { Link } from "react-router-dom"
import { authStore } from "../../../stores/AuthStore"
import { observer } from "mobx-react"
import { socket2 } from "../../../socket"
import { gameStore } from "../../../stores/GameStore"

const SidebarToggle = ({click, active}) => {

    const [toggled, setToggled] = useState(false)

    const toggleClasses = [classes.toggle, classes.logo]
    if (toggled) {toggleClasses.push(classes.toggle_active)}

    const callback = () => {
        active ? click(false) : click(true)
        toggled ? setToggled(false) : setToggled(true)
    }

    return (
        <li className={classes.lilogo}>
            <div className={classes.item}>
                <span className={toggleClasses.join(" ")} onClick={() => callback()}></span>
                <span className={classes.text}>21.Game</span>
            </div>
        </li>
    )
}

const Item = ({children, icon, link, ...props}) => {
    return (
        <li>
            <Link to={link} className={classes.item} {...props}>
                <span  className={classes.icon_span}><img src={icon} alt=""  className={classes.icon}></img></span>
                <span className={classes.text}>{children}</span>
            </Link>
        </li>
    )
}

const Sidebar = observer(() => {
    const isAuth = authStore.selectIsAuth()
    const [barActive, setBarActive] = useState(false)

    const searchGame = () => {
        socket2.connect()
    }

    const click = (barActive) => {
        setBarActive(barActive)
    }

    const logout = () => {
        if (window.confirm("Вы действительно хотите выйти?")) {
            authStore.logout()
            window.localStorage.removeItem("token")
        }
    }

    const sidebarClasses = [classes.sidebar]
    if (barActive) {sidebarClasses.push(classes.active)}


    return (
        <>
        <div className={sidebarClasses.join(" ")}> 
            <ul>
                <SidebarToggle click={click} active={barActive}/>
                <Item icon={homeImg} link={"/"}>Главная</Item>
                <Item icon={aboutImg} link={"/about"}>Об игре</Item>
                <Item icon={aboutImg} link={"/aa"}>??? 😀😀</Item>
                {!isAuth ? <>
                    <div className={classes.bottom_notAuthed}>
                        <Item icon={loginImg} link={"/auth"}>Войти</Item>
                    </div>
                </>:
                gameStore.inGame ?
                <>
                <Item icon={profImg}>Профиль</Item>
                <Item icon={searchImg} onClick={() => searchGame()} >Быстрая игра</Item>
                <div className={classes.bottom_authed}>
                    <Item icon={profileImg} style={{}} link={"/profile"}>{authStore.data.name}</Item>
                    <Item icon={logoutImg} onClick={() => logout()}>Logout</Item>
                </div>
                </>
                :                 <>
                <Item icon={profImg}>Профиль</Item>
                <Item icon={searchImg} link={"/search"} onClick={() => searchGame()} >Быстрая игра</Item>
                <div className={classes.bottom_authed}>
                    <Item icon={profileImg} style={{}} link={"/profile"}>{authStore.data.name}</Item>
                    <Item icon={logoutImg} onClick={() => logout()}>Logout</Item>
                </div>
                </>
                }

                
            </ul>
        </div>
        </>
    )
})

export default Sidebar