import classes from "./Chat.module.css"
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import React, { useRef } from "react";
import { observer } from "mobx-react";
import { useForm } from "react-hook-form";
import { messagesStore } from "../../../stores/MessagesStore";
import { authStore } from "../../../stores/AuthStore";
import { useEffect, useState } from "react";
import { socket } from "../../../socket";
import { action, autorun } from "mobx";


const MessageForm = () => {
    


    socket.on("connect", () => {
        console.log("connected")
    })

    const { register, handleSubmit, resetField} = useForm({
        defaultValues: {
            message: "",
        }
    })

    const onSubmit = action((values) => {
        resetField("message")
        socket.emit("messageTo", {author: authStore.data.name, text: values.message, color: authStore.data.color, isBanned: false})
    })

    const sentMessage = action((msg) => {
        messagesStore.messages.push(msg)
    })

    useEffect(() => {
        socket.on("messageFrom", sentMessage)
        return () => {
            socket.off("messageFrom", sentMessage)
        }
    })




    return (
        <Form className={classes.messages_form} onSubmit={handleSubmit(onSubmit)}>
            <div className={classes.btn_wrap}>
                <Button variant="primary" type="submit" className={classes.submit_message_button}>
                    <div className={classes.send_img}/>
                </Button>
            </div>
            <Form.Group className={classes.input_cont}>
                <Form.Control className={classes.input_message} placeholder="сообщение..." {...register("message", {required: "Сообщение пустое"})}/>
                {/* <Form.Control className={classes.input_message} placeholder="сообщение..."/> */}
            </Form.Group>
        </Form>
    )
}

const Message = ({author, text, color}) => {

    // const isAdmin = authStore.selectIsAdmin()
    const isAdmin = false

    // const [modal, setModal] = useState(false);
    // const [btnCoord, setBtnCoord] = useState("")
    // const logRef = useRef();
    const UserModal = ({onChange, close}) => {

        const handleBan = () => {
            socket.emit("banUser", author)
        }

        const handleDelete = () => {
            onChange(true) // callback-функция
        }

        const handleClose = () => {
            close(false)
        }

        return (
            <>
            <div className={classes.modal_cont}>
                {/* <div className={classes.modal_btns} onClick={handleDelete}>
                    Удалить сообщение
                </div> */}
                <div className={classes.modal_btns} onClick={() => handleBan()}>
                    Забанить
                </div>
                <div className={classes.modal_btns} onClick={handleClose}>
                    Закрыть
                </div>
    
            </div>
             </>
        )
    }

    
    const [hover, setHover] = useState(false)
    const [deleted, setDeleted] = useState(false)
    
    const handleDelete = (value) => {
        setDeleted(value)
    }
    const handleClose = (value) => {
        setHover(value)
    }
    // const openAuthModal = () => {
    //     setBtnCoord(logRef.current.getBoundingClientRect())
    //     modal ? setModal(false) : setModal(true)
    // }

    const openAuthModal = () => {
        setHover(<UserModal onChange={handleDelete} close={handleClose}/>)
    }

    return (
        
        <>
        {/* <UserModal btnCoord={btnCoord} active={modal}/> */}

        <div className={classes.message_cont}>
            {hover}
            {isAdmin ? <div className={`${classes.message_author} ${classes.clickable}`} style={{color: color}} onClick={() => openAuthModal()}>{author}:</div> 
            :
            <div className={classes.message_author} style={{color: color}} >{author}:</div>}
            {deleted ? 
            <div className={classes.message}>{"<Сообщение удалено>"}</div>
            :
            <div className={classes.message}>{text}</div>}
            </div>
        </>

    )
}

const Chat = observer(() => {



    autorun(() => {
        scrollToBottom()
    })



    const messagesEnd = React.useRef()

    const scrollToBottom = () => {
        messagesEnd.current?.scrollIntoView({ behavior: "smooth" });
    }

    const [status] = useState(socket.connected)
    // const [status, setStatus] = useState(false)


    if (messagesStore.messages.length > 50) {
        messagesStore.messages.shift()
    }

    return (
        <> 
        <div className={classes.chat_main_cont}>
            {/* {status ? <div>Online</div> : <div>Offline</div>} */}
            <div className={classes.messages_cont}>
                {messagesStore.messages[0] ? messagesStore.messages.map((message, index) => 
                    <Message author={message.author} color={message.color} text={message.text} key={index}/>
                )
                 :
                <div className={classes.no_messages}>Сообщений нет...</div>
                }
                <div style={{ float:"left", clear: "both" }}
                    ref={messagesEnd}>
                </div>
            </div>
            <MessageForm/>
        </div>
        </>
    )
})

export default Chat